import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

import * as math from "mathjs";

class Taschenrechner extends React.Component {
  constructor(props){
    super(props);
    this.state={
      content: "",
    };
  }

  handleClick(i){
    this.setState({content: this.state.content + i}); 
  }

  handleClear(){
    this.setState({content: ""}); 
  }

  handleEqual(){
    this.setState({content: math.evaluate(this.state.content) });
  }
  

  render(){

    return ( 
      <View style={{flex:1, flexDirection:"column", justifyContent: 'center',}}>
        <View>
          <Text>{this.state.content}</Text>
        </View>
        <View style={{flexDirection:"row", justifyContent: 'center', height: 50}} >
          <Button title="7" onPress={()=>this.handleClick("7")} />
          <Button title="8" onPress={()=>this.handleClick("8")}/>
          <Button title="9" onPress={()=>this.handleClick("9")}/>
          <Button title=":" onPress={()=>this.handleClick("/")}/>
          <Button title="C" onPress={()=>this.handleClear()}/>
        </View>

        <View style={{flexDirection:"row", justifyContent: 'center', height: 50}} >
          <Button title="4" onPress={()=>this.handleClick("4")}/>
          <Button title="5" onPress={()=>this.handleClick("5")}/>
          <Button title="6" onPress={()=>this.handleClick("6")}/>
          <Button title="x" onPress={()=>this.handleClick("*")}/>
          <Button title="(" onPress={()=>this.handleClick("(")}/>
        </View>

        <View style={{flexDirection:"row", justifyContent: 'center', height: 50}} >
          <Button title="1" onPress={()=>this.handleClick("1")}/>
          <Button title="2" onPress={()=>this.handleClick("2")}/>
          <Button title="3" onPress={()=>this.handleClick("3")}/>
          <Button title="-" onPress={()=>this.handleClick("-")}/>
          <Button title=")" onPress={()=>this.handleClick(")")}/>
        </View>

        <View style={{flexDirection:"row", justifyContent: 'center', height: 50}} >
          <Button title="0" onPress={()=>this.handleClick("0")}/>
          <Button title="," onPress={()=>this.handleClick(".")}/>
          <Button title="+" onPress={()=>this.handleClick("+")}/>
          <Button title="=" onPress={()=>this.handleEqual()}/>
        </View>

      </View>
      
    );
  }
}

export default function App() {
  return (
    <View style={styles.container}>
      <Taschenrechner />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
